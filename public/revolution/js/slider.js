$(document).ready(function() {
    /* initialize the slider based on the Slider's ID attribute */
    $('#home_slider').show().revolution({
        /* options are 'auto', 'fullwidth' or 'fullscreen' */
        sliderLayout: 'auto',

        /* basic navigation arrows and bullets */
        navigation: {
            bullets: {
                enable: true,
                style: "hermes",
                hide_onleave: false,
                h_align: "center",
                v_align: "bottom",
                h_offset: 0,
                v_offset: 20,
                space: 5
            }
        },

        responsiveLevels: [1240, 1024, 778, 480],
 
        /* [DESKTOP, LAPTOP, TABLET, SMARTPHONE] */
        gridwidth:[1240, 1024, 778, 480],
     
        /* [DESKTOP, LAPTOP, TABLET, SMARTPHONE] */
        gridheight:[400, 768, 960, 720]
    });

    /* initialize the slider based on the Slider's ID attribute */
    $('#services_slider').show().revolution({
        /* options are 'auto', 'fullwidth' or 'fullscreen' */
        sliderLayout: 'auto',

        /* basic navigation arrows and bullets */
        navigation: {
            arrows: {
                enable: true,
                style: 'hesperiden',
                hide_onleave: false
            }
        }
    });
});