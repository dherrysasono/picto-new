// declaration textarea names
const textareaNames = ['inputDescription'];

textareaNames.forEach(function(textareaName) {
	$("#" + textareaName).summernote({
		height : 300
	});
});