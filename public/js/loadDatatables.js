const tableNames = ['dtWhatWeDo','dtOurValues','dtBackdropMaterial'];
var table;

tableNames.forEach(function(tableName) {
	table = $('#' + tableName).DataTable({
		ajax : "/" + base_url[1] + "/datatables/" + tableName
	});
});