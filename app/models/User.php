<?php
class User {
    private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }

    // Register user
    public function register($data)
    {
        $this->db->query('Insert into users (name, email, password) VALUES (:name, :email, :password)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    // Login user
    public function login($email,$password)
    {
        $this->db->query('Select * from users where email = :email');
        $this->db->bind(':email',$email);

        $row = $this->db->singleData();

        $hashed_password = $row->password;
        if(password_verify($password, $hashed_password)){
            return $row;
        } else {
            return false;
        }
    }


    // Find user by email
    public function findUserByEmail($email)
    {
        $this->db->query('select * from users where email = :email');
        $this->db->bind(':email', $email);
    
        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount()>0){
            return true;
        } else {
            return false;
        }
    }

    // Find user by id
    public function getUserByID($id)
    {
        $this->db->query('select * from users where id = :id');
        $this->db->bind(':id', $id);
    
        $row = $this->db->singleData();

       return $row;
    }

    public function getAppUserByUsername($username)
    {
        $this->db->query('SELECT * FROM user WHERE userName = :username');
        $this->db->bind(':username', $username);
    
        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }
}