<?php
    class Site {
        private $db;

        public function __construct(){
            $this->db = new Database;
        }

        public function getSites($active=1){
            $this->db->query('SELECT * FROM "MS_Site" WHERE "isActive" = :isActive');
            $this->db->bind(':isActive', $active);

            $result = $this->db->resultSet();

            return $result;
        }  
        
        public function addSite($data){
            $this->db->query('Insert into MS_Site (siteCode, siteName, location, remarks, isActive, insertTime, insertUserID, lastUpdated, lastUpdaterID) VALUES (:siteCode, :siteName, :location, :remarks, :isActive, :insertTime, :insertUserID, :lastUpdated, :lastUpdaterID)');
            $this->db->bind(':siteCode', $data['siteCode']);
            $this->db->bind(':siteName', $data['siteName']);
            $this->db->bind(':location', $data['location']);
            $this->db->bind(':remarks', $data['remarks']);
            $this->db->bind(':isActive', $data['isActive']);
            $this->db->bind(':insertTime', $data['insertTime']);
            $this->db->bind(':insertUserID', $data['insertUserID']);
            $this->db->bind(':lastUpdated', $data['lastUpdated']);
            $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

        public function updateSite($data){
            $this->db->query('Update MS_Site set siteCode= :siteCode, siteName= :siteName, location= :location, remarks= :remarks, isActive= :isActive, lastUpdated= :lastUpdated, lastUpdaterID= :lastUpdaterID where siteID = :siteID');
            $this->db->bind(':siteID', $data['siteID']);
            $this->db->bind(':siteCode', $data['siteCode']);
            $this->db->bind(':siteName', $data['siteName']);
            $this->db->bind(':location', $data['location']);
            $this->db->bind(':remarks', $data['remarks']);
            $this->db->bind(':isActive', $data['isActive']);
            $this->db->bind(':lastUpdated', $data['lastUpdated']);
            $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

        public function getSiteByID($id){
            $this->db->query('Select * from MS_Site where siteID= :siteID');
            $this->db->bind(':siteID',$id);

            $row =  $this->db->singleData();

            return $row;
        }

        public function deleteSite($id){
            $this->db->query('Update MS_Site set isActive= :isActive, lastUpdated= :lastUpdated, lastUpdaterID= :lastUpdaterID where siteID = :siteID');
            $this->db->bind(':siteID', $data['siteID']);
            $this->db->bind(':isActive', 0);
            $this->db->bind(':lastUpdated', $data['lastUpdated']);
            $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

    }