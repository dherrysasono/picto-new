<?php defined('URLROOT') OR exit('No direct script access allowed');

class Datatable {

	private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }

    public function getDataServices($servicesGroup)
    {
        $this->db->query('	SELECT servicescategory.categoryName, services.servicesID, services.servicesTitle,services.servicesDesc,
        					services.lastUpdated, user.userName
        					FROM services_category
        				  	INNER JOIN servicescategory ON servicescategory.categoryID=services_category.categoryID
        				  	INNER JOIN services ON services.servicesID=services_category.servicesID
        				  	INNER JOIN user ON user.userID=services.lastUpdaterID
        				  	WHERE services.servicesGroup = :servicesGroup');
        $this->db->bind(':servicesGroup', $servicesGroup);
        
        $row = $this->db->resultSetArray();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }
}