<?php
    class Post {
        private $db;

        public function __construct(){
            $this->db = new Database;
        }

        public function getPosts(){
            $this->db->query('SELECT *, posts.id as postId, users.id as userid, posts.created_at as postCreated, users.created_at as userCreated from posts
            INNER JOIN users on posts.user_id = users.id
            ORDER BY posts.created_at DESC');

            $result = $this->db->resultSet();

            return $result;
        }  
        
        public function addPost($data){
            $this->db->query('Insert into posts (title, user_id, body) VALUES (:title, :user_id, :body)');
            $this->db->bind(':title', $data['title']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':body', $data['body']);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

        public function updatePost($data){
            $this->db->query('Update posts set title= :title, body = :body where id = :id');
            $this->db->bind(':title', $data['title']);           
            $this->db->bind(':body', $data['body']);
            $this->db->bind(':id', $data['id']);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

        public function getPostByID($id){
            $this->db->query('Select * from posts where id= :id');
            $this->db->bind(':id',$id);

            $row =  $this->db->singleData();

            return $row;
        }

        public function deletePost($id){
            $this->db->query('Delete from posts where id = :id');
            $this->db->bind(':id', $id);

            // Execute
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

    }