<?php
class Menu {
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getParentMenu($projectID, $rolesID, $menuID = '')
    {
        if ($menuID) {
            $this->db->query('select "APP_Menu"."projectID","APP_Menu"."menuID","APP_Menu"."menuName","APP_Menu"."menuPath",
                            "APP_Menu"."menuPage","APP_Menu"."menuIcon","APP_MenuPermission"."rolesID"
                            from "APP_MenuPermission"
                            inner join "APP_Menu" ON "APP_Menu"."projectID"="APP_MenuPermission"."projectID" AND 
                            "APP_Menu"."menuID"="APP_MenuPermission"."menuID" 
                            where "APP_MenuPermission"."projectID" = :projectID AND 
                            "APP_MenuPermission"."rolesID" = :rolesID AND 
                            "APP_Menu"."parentID" = :menuID');
            $this->db->bind(':projectID', $projectID);
            $this->db->bind(':rolesID', $rolesID);
            $this->db->bind(':menuID', $menuID);
        } else {
            $this->db->query('select "APP_Menu"."projectID","APP_Menu"."menuID","APP_Menu"."menuName","APP_Menu"."menuPath",
                            "APP_Menu"."menuPage","APP_Menu"."menuIcon","APP_MenuPermission"."rolesID" 
                            from "APP_MenuPermission"
                            inner join "APP_Menu" ON "APP_Menu"."projectID"="APP_MenuPermission"."projectID" AND 
                            "APP_Menu"."menuID"="APP_MenuPermission"."menuID" 
                            where "APP_MenuPermission"."projectID" = :projectID AND 
                            "APP_MenuPermission"."rolesID" = :rolesID AND 
                            "APP_Menu"."parentID" is NULL');
            $this->db->bind(':projectID', $projectID);
            $this->db->bind(':rolesID', $rolesID);
        }
    
        $row = $this->db->resultSetArray();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }
}       