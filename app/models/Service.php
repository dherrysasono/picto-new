<?php defined('URLROOT') OR exit('No direct script access allowed');

class Service {

	private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }

    // Get data services by id
    public function getServiceByID($serviceID)
    {
        $this->db->query('SELECT services.*, servicescategory.categoryName, servicesimages.imageTitle, servicesimages.imageDesc,
                            servicesimages.imageID 
        					FROM services 
        					INNER JOIN services_category ON services_category.servicesID=services.servicesID 
        					INNER JOIN servicescategory ON servicescategory.categoryID=services_category.categoryID 
                            LEFT JOIN servicesimages ON servicesimages.servicesID=services.servicesID 
        					WHERE services.servicesID = :servicesID');
        $this->db->bind(':servicesID', $serviceID);
    
        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount()>0){
            return $row;
        } else {
            return false;
        }
    }

    // update table services
    public function updateServices($data)
    {
        $sql = "UPDATE services SET servicesTitle=:servicesTitle, servicesDesc=:servicesDesc, lastUpdated=:lastUpdated, lastUpdaterID=:lastUpdaterID WHERE servicesID=:servicesID";
        $this->db->query($sql);
        
        if($this->db->execute($data)){
            return true;
        } else {
            return false;
        }
    }

    // update table services
    public function updateServicesImage($data)
    {
        $sql = "UPDATE servicesimages SET servicesID=:servicesID, imageTitle=:imageTitle, imageDesc=:imageDesc, lastUpdated=:lastUpdated, lastUpdaterID=:lastUpdaterID WHERE imageID=:imageID";
        $this->db->query($sql);
        $this->db->bind(':servicesID', $data['servicesID']);           
        $this->db->bind(':imageTitle', $data['imageTitle']);
        $this->db->bind(':imageDesc', $data['imageDesc']);
        $this->db->bind(':lastUpdated', $data['lastUpdated']);
        $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);
        $this->db->bind(':imageID', $data['imageID']);
        
        if($this->db->execute($data)){
            return true;
        } else {
            return false;
        }
    }
}