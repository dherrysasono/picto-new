<?php defined('URLROOT') OR exit('No direct script access allowed');

class Page {

	private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }

    public function getServiceTypes()
    {
    	$query = "SELECT * FROM servicescategory";
    	$this->db->query($query);
    	$results = $this->db->resultSet();

    	// check row
        if($this->db->rowCount() > 0){
            return $results;
        } else {
            return false;
        }
    }

    public function getWhatWeDo($categoryID)
    {
        $query = "SELECT services_category.servicesID, services.servicesTitle, services.servicesDesc, servicesimages.imageTitle,
                     servicesimages.imageDesc 
                    FROM services_category 
                    INNER JOIN services ON services.servicesID=services_category.servicesID 
                    LEFT JOIN servicesimages ON services.servicesID=servicesimages.servicesID 
                    WHERE services_category.categoryID=:categoryID AND services.servicesGroup=:servicesGroup";
        $this->db->query($query);
        $this->db->bind(':categoryID', $categoryID);
        $this->db->bind(':servicesGroup', 'What We Do');

        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function getOurValues($categoryID)
    {
        $query = "SELECT services_category.servicesID, services.servicesTitle, services.servicesDesc, servicesimages.imageTitle,
                     servicesimages.imageDesc 
                    FROM services_category 
                    INNER JOIN services ON services.servicesID=services_category.servicesID 
                    LEFT JOIN servicesimages ON services.servicesID=servicesimages.servicesID 
                    WHERE services_category.categoryID=:categoryID AND services.servicesGroup=:servicesGroup";
        $this->db->query($query);
        $this->db->bind(':categoryID', $categoryID);
        $this->db->bind(':servicesGroup', 'Our Values');

        $results = $this->db->resultSet();

        // check row
        if($this->db->rowCount() > 0) {
            return $results;
        } else {
            return false;
        }
    }
}