<?php
class Project {
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getDataProject()
    { 
        $this->db->query('SELECT "MS_Project".*, "APP_User".username FROM "MS_Project"
                        INNER JOIN "APP_User" ON "APP_User"."userID"="MS_Project"."insertUserID" 
                        WHERE "MS_Project"."isActive" = :isActive');
        $this->db->bind(':isActive', '1');
        
        $row = $this->db->resultSetArray();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }

    public function getLastProjectCode()
    { 
        $this->db->query('SELECT "projectCode" FROM "MS_Project" ORDER BY "projectID" DESC');
        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }

    public function addProject($data){
        $this->db->query('Insert into "MS_Project" ("siteID", "projectCode", "projectName", remarks, "isActive", 
        "insertTime", "insertUserID", "lastUpdated", "lastUpdaterID") VALUES (:siteID, :projectCode, :projectName, :remarks, :isActive, :insertTime, 
        :insertUserID, :lastUpdated, :lastUpdaterID)');
        $this->db->bind(':siteID', $data['siteID']);
        $this->db->bind(':projectCode', $data['projectCode']);
        $this->db->bind(':projectName', $data['projectName']);
        $this->db->bind(':remarks', $data['remarks']);
        $this->db->bind(':isActive', $data['isActive']);
        $this->db->bind(':insertTime', $data['insertTime']);
        $this->db->bind(':insertUserID', $data['insertUserID']);
        $this->db->bind(':lastUpdated', $data['lastUpdated']);
        $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function updateProject($data){
        $this->db->query('UPDATE "MS_Project"
                            SET "siteID"=:siteID, "projectCode"=:projectCode, "projectName"=:projectName, remarks=:remarks, 
                            "isActive"=:isActive, "insertTime"=:insertTime, "insertUserID"=:insertUserID, "lastUpdated"=:lastUpdated, 
                            "lastUpdaterID"=:lastUpdaterID
                            WHERE "projectID"= :projectID');
        $this->db->bind(':projectID', $data['projectID']);
        $this->db->bind(':siteID', $data['siteID']);
        $this->db->bind(':projectCode', $data['projectCode']);
        $this->db->bind(':projectName', $data['projectName']);
        $this->db->bind(':remarks', $data['remarks']);
        $this->db->bind(':isActive', $data['isActive']);
        $this->db->bind(':insertTime', $data['insertTime']);
        $this->db->bind(':insertUserID', $data['insertUserID']);
        $this->db->bind(':lastUpdated', $data['lastUpdated']);
        $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function deleteProject($data){
        $this->db->query('UPDATE "MS_Project"
                            SET "isActive"=:isActive, "lastUpdated"=:lastUpdated,
                            "lastUpdaterID"=:lastUpdaterID
                            WHERE "projectID"= :projectID');
        $this->db->bind(':projectID', $data['projectID']);
        $this->db->bind(':isActive', $data['isActive']);
        $this->db->bind(':lastUpdated', $data['lastUpdated']);
        $this->db->bind(':lastUpdaterID', $data['lastUpdaterID']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function getProjectByID($projectID)
    { 
        $this->db->query('SELECT * FROM "MS_Project" WHERE "projectID" = :projectID');
        $this->db->bind(':projectID', $projectID);

        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }

    public function getProjectByParams($data)
    {
        if (isset($data['projectID'])) {
            $this->db->query('SELECT * FROM "MS_Project" WHERE "projectID" = :projectID AND 
            "siteID" = :siteID AND "projectCode" = :projectCode AND "projectName" = :projectName');

            $this->db->bind(':projectID', $data['projectID']);    
            $this->db->bind(':projectCode', $data['projectCode']);
        } else {
            $this->db->query('SELECT * FROM "MS_Project" WHERE "siteID" = :siteID AND 
            "projectName" = :projectName');
        }
 
        $this->db->bind(':siteID', $data['siteID']);    
        $this->db->bind(':projectName', $data['projectName']);
        
        $row = $this->db->singleData();

        // check row
        if($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}