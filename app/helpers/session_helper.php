<?php
session_start();

function isLoggedIn()
{
    if(isset($_SESSION['userID'])) {
       return true;
    } else {
       return false;
    }
}

function set_flashdata($name = '', $message = '')
{
    if (!isset($_SESSION[$name]) & $name != '' & $message != '') {
        $_SESSION[$name] = $message;
    }
}

function flashdata($name = '')
{
    if (isset($_SESSION[$name]) & $name != '') {
        $value = $_SESSION[$name];
        $_SESSION['sh_flash'] = $value;
        unset($_SESSION[$name]);
    } else {
        $_SESSION['sh_flash'] = FALSE;
    }

    return $_SESSION['sh_flash'];
}

function set_userdata($data, $value = NULL)
{
    if (is_array($data)) {
        foreach ($data as $key => &$value) {
            $_SESSION[$key] = $value;
        }

        return;
    }

    $_SESSION[$data] = $value;
}

function unset_userdata($key)
{
    if (is_array($key)) {
        
        foreach ($key as $k) {
            unset($_SESSION[$k]);
        }

        return;
    }
    
    unset($_SESSION[$key]);
}