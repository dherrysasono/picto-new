<?php require APPROOT . '/views/inc/admin_header.php'; ?>

<div class="container-fluid">
	<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        	<a href="#"><?php echo $data['parentTitle']; ?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo $data['title']; ?></li>
    </ol>

    <div class="card mb-3">
    	<div class="card-header">
    		<i class="fas fa-pencil"></i>
            <?php echo $data['title']; ?>
        </div>
        <div class="card-body">
        	<div class="row">
		    	<div class="col-sm-12">
                    <?php if (!empty($data['errors'])) {
                        foreach ($data['errors'] as $row) {
                            echo "  <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                        <strong>Image error!</strong> " . $row ."
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                        </button>
                                    </div>";
                        }
                    }
                    ?>
		    		<form method="post" enctype="multipart/form-data">
					  	<div class="form-group">
					    	<label for="inputCategoryName">Category Name</label>
					    	<input type="text" class="form-control" id="inputCategoryName" name="categoryName" required="required" value="<?php echo $data['services']->categoryName; ?>" readonly>
					  	</div>
					  	<div class="form-group">
					    	<label for="inputServicesTitle">Service Name</label>
					    	<input type="text" class="form-control" id="inputServicesTitle" name="servicesTitle" required="required" value="<?php echo $data['services']->servicesTitle; ?>">
					  	</div>
                        <div class="form-group">
                            <label for="inputDescription">Description</label>
                            <textarea class="form-control" id="inputDescription" name="serviceDescription"><?php echo $data['services']->servicesDesc; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputImageName">
                                <?php
                                if ($data['services']->imageDesc) {
                                    echo "<img src='".URLROOT.$data['services']->imageDesc."' alt='".$data['services']->imageTitle."' class='img-thumbnail'>";
                                }
                                ?>
                            </label>
                            <input type="file" class="form-control" id="inputImageName" name="image">
                            <input type="hidden" name="imageID" value="<?php echo $data['services']->imageID; ?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?php echo URLROOT; ?>/whatwedos" type="button" class="btn btn-warning">Cancel</a>
					</form>
		    	</div>
		    </div>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/inc/admin_footer.php' ; ?>