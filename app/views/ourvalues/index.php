<?php require APPROOT . '/views/inc/admin_header.php'; ?>

<div class="container-fluid">
	<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        	<a href="#"><?php echo $data['parentTitle']; ?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo $data['title']; ?></li>
    </ol>

    <div class="card mb-3">
    	<div class="card-header">
    		<i class="fas fa-table"></i>
            <?php echo $data['title']; ?>
        </div>
        <div class="card-body">
            <?php if (!empty($data['success_message'])) {
                echo "  <div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <strong>Successfull!</strong> " . $data['success_message'] ."
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
            }
            ?>
            <?php if (!empty($data['error_message'])) {
                echo "  <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <strong>Error!</strong> " . $data['error_message'] ."
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
            }
            ?>
        	<div class="table-responsive">
                <table class="table table-bordered" id="dtOurValues" width="100%" cellspacing="0">
                	<thead>
                    	<tr>
                      		<th style="width: 25%;">Category</th>
                      		<th style="width: 35%;">Description</th>
                      		<th style="width: 20%;">Last Updated</th>
                      		<th style="width: 15%;">Last Updater</th>
                      		<th style="width: 5%;"></th>
                    	</tr>
                  	</thead>
                  	<tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/inc/admin_footer.php' ; ?>