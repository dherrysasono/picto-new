<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $data['title']; ?></title>

        <!-- Bootstrap core CSS-->
        <link href="<?php echo URLROOT; ?>/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="<?php echo URLROOT; ?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Custom styles for this template-->
        <link href="<?php echo URLROOT; ?>/css/sb-admin.css" rel="stylesheet">
    </head>
    <body class="bg-navyblue">
        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <?php if ($data['error_message']) {
                        echo "<div class='bs-component'>
                                <div class='alert alert-dismissible alert-danger'>
                                    <button class='close' type='button' data-dismiss='alert'>×</button>
                                    " . $data['error_message'] . "
                                </div>
                            </div>";
                    }
                    ?>
                    <form method="post">
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required>
                                <label for="username">Username</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="<?php echo URLROOT; ?>/js/jquery.min.js"></script>
        <script src="<?php echo URLROOT; ?>/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?php echo URLROOT; ?>/js/jquery.easing.js"></script>
    </body>
</html>