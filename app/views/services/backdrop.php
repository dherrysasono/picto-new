<section class="flat-row section-icon">
    <div class="container">
    	<div class="row">
		    <div class="col-sm-5">
		        <img src="<?php echo URLROOT; ?>/images/whatwedo.jpg" class="img-fluid">
		    </div>
		    <div class="col-sm-7">
		        <div class="title-section style3 left">
		            <h1 class="title picto-text">What We Do</h1>
		            <p class="text-justify picto-text" style="color : #231F20;">
		                Salah satu unit bisnis yang ditawarkan oleh <span style="color:#E6A31E">Pictograph</span> adalah mengerjakan <span style="color:#E6A31E">pembuatan dan sewa backdrop & ﬂooring</span> untuk berbagai acara : seminar, product launching, gathering, RUPS dll dan kami juga sudah menangani berbagai client besar seperti : lippo cikarang, orange county, meikarta, japan information center, LRT Ciputat dll.
		            </p>
		        </div>
		    </div>
		</div>
    </div>
</section>

<section class="flat-row v12 parallax parallax5">
	<div class="section-overlay style2"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title-section style3 text-center">
                    <h1 class="title" style="color:#fff">Why You Choose Us</h1>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-star" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Kami memberikan kualitas material terbaik dengan menggunakan kerangka besi agar backdrop dan flooring lebih kuat dan rapih, ditambah dengan hasil cetakan high resolution.
                    	</p> 
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-map-marker" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Jangkauan area yang kami layani cukup luas yaitu Jakarta, Bogor, Depok, Tanggerang dan Bekasi sehingga pelanggan tidak perlu khawatir untuk mengadakan event di area tersebut.
                    	</p> 
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-money" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Harga yang kami tawarkan sangat terjangkau dan kompetitif sehingga pelanggan dapat merasakan benefit yang diberikan dan harga kami sudah termasuk jasa bongkar & pasang, transportasi, satu set lampu, cetak materi dan karpet.
                    	</p> 
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-cog" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Konstruksi backdrop/flooring milik kami sudah berbentuk per modul sehingga lebih cepat dan memudahkan kami dalam melakukan perakitan dilokasi.
                    	</p> 
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-phone" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Tim kami dengan senang hati membantu anda jika ada pertanyaan terkait produk atau perusahaan baik sebelum, selama dan sesudah acara dan dari sisi teknis ataupun administrasi
                    	</p> 
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon" style="background-color: #12172b; border: 3px solid #f2c21a;">
                            <i class="fa fa-male" aria-hidden="true" style="color: #f2c21a"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify white-text">
                        Kami memiliki tim yang berpengalaman yang sudah menangani acara besar seperti RUPS, gathering, product launching, konserpress conference dll
                    	</p> 
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>