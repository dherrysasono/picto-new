				<!-- Sticky Footer -->
        		<footer class="sticky-footer">
          			<div class="container my-auto">
            			<div class="copyright text-center my-auto">
              				<span>Copyright © Your Website 2018</span>
            			</div>
          			</div>
        		</footer>

			</div><!-- /.content-wrapper -->
		</div><!-- /#wrapper -->

		<!-- Scroll to Top Button-->
    	<a class="scroll-to-top rounded" href="#page-top">
      		<i class="fas fa-angle-up"></i>
    	</a>

    	<!-- Logout Modal-->
    	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      		<div class="modal-dialog" role="document">
        		<div class="modal-content">
          			<div class="modal-header">
            			<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            			<button class="close" type="button" data-dismiss="modal" aria-label="Close">
              				<span aria-hidden="true">×</span>
            			</button>
          			</div>
          			<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          			<div class="modal-footer">
            			<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            			<a class="btn btn-primary" href="<?php echo URLROOT; ?>/users/logout">Logout</a>
          			</div>
        		</div>
      		</div>
    	</div>
    	
		<!-- Bootstrap core JavaScript-->
		<script src="<?php echo URLROOT; ?>/js/jquery.min.js"></script>
	    <script src="<?php echo URLROOT; ?>/js/bootstrap.bundle.min.js"></script>

	    <!-- Core plugin JavaScript-->
	    <script src="<?php echo URLROOT; ?>/js/jquery.easing.js"></script>

	    <!-- Page level plugin JavaScript-->
	    <script src="<?php echo URLROOT; ?>/js/plugins/jquery.dataTables.min.js"></script>
    	<script src="<?php echo URLROOT; ?>/js/plugins/dataTables.bootstrap4.min.js"></script>

    	<!-- Custom scripts for all pages-->
    	<script src="<?php echo URLROOT; ?>/js/sb-admin.min.js"></script>

	    <script type="text/javascript">
			var base_url = location.pathname.split('/');
		</script>

	    <!-- Another JS -->
	    <?php
	    if (isset($data['javascript'])) {
	    	foreach ($data['javascript'] as $javascript) {
	    		echo '<script src="'.URLROOT.'/js/'.$javascript.'"></script>';
	    	}
	    }
	    ?>
  	</body>
</html>