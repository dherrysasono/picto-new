<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
    <head>
        <meta charset="utf-8">
        <title><?php echo SITENAME; ?></title>
        <meta name="author" content="strawheads team">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/responsive.css">

        <?php if(isset($data['color'])): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/colors/color6.css" id="colors">
        <?php else: ?>
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/colors/color4.css" id="colors">
        <?php endif; ?>

        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/animate.css">

        <?php if(!isset($data['color'])): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/headline.css">
        <?php endif; ?>
        
        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT;?>/css/pictograph.css">

        <!-- Owl Carousel -->
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/owl.theme.default.min.css">

        <link href="icon/apple-touch-icon-48-precomposed.png" rel="icon" sizes="48x48">
        <link href="icon/apple-touch-icon-32-precomposed.png" rel="icon">
        <link href="icon/favicon.png" rel="icon">
    </head>
    <body class="header_sticky">
        <!-- Preloader -->
        <div id="loading-overlay">
            <div class="loader"></div>
        </div>

        <!-- Boxed -->
        <div class="boxed">
            <div class="flat-header-wrap">
                <!-- Header -->            
                <div class="header widget-header widget-header-style2 clearfix">
                    <div class="container">
                        <div class="header-wrap clearfix">
                            <div class="row"> 
                                <div class="col-lg-3">
                                    <div id="logo" class="logo">
                                        <a href="<?php echo URLROOT; ?>" rel="home">
                                            <img src="<?php echo URLROOT; ?>/images/logo-pictograph.png" alt="image">
                                        </a>
                                    </div><!-- /.logo -->
                                </div>
                                <div class="col-lg-9">
                                    <div class="wrap-widget-header clearfix">
                                        <aside class="widget widget-info color35d">          
                                            <div class="textwidget clearfix">
                                                <div class="info-icon">
                                                    <img src="<?php echo URLROOT; ?>/icon/map-marker.png" alt="image">
                                                </div>
                                                <div class="info-text">
                                                    <h6 class="contact-us">Ofﬁce & Workshop :</h6>
                                                    <p class="contact-us">Jl. Rawa Ajan No. 115</p>
                                                    <p class="contact-us">Bantar Gebang, Bekasi</p>
                                                </div>
                                            </div>
                                        </aside>
                                        <aside class="widget widget-info color35d">          
                                            <div class="textwidget clearfix">
                                                <div class="info-icon">
                                                    <img src="<?php echo URLROOT; ?>/icon/email.png" alt="image">
                                                </div>
                                                <div class="info-text">
                                                    <h6 class="contact-us" style="margin-top: 10px">pictograph.info@gmail.com</h6>
                                                </div>
                                            </div>
                                        </aside>
                                        <aside class="widget widget-info color35d">          
                                            <div class="textwidget clearfix">
                                                <div class="info-icon">
                                                    <img src="<?php echo URLROOT; ?>/icon/phone.png" alt="image">
                                                </div>
                                                <div class="info-text">
                                                    <h6 class="contact-us" style="margin-top: 10px">0852 1374 0851</h6>
                                                </div>
                                            </div>
                                        </aside>               
                                    </div>
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.header-wrap -->
                    </div>
                </div><!-- /.header -->

                <?php require APPROOT .'/views/inc/navbar.php'; ?>
            </div><!-- /.flat-header-wrap -->