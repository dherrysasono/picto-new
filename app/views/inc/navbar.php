<header id="header" class="header header-style1 header-classic">
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-wrap-header">
                    <div class="nav-wrap clearfix">        
                        <nav id="mainnav" class="mainnav color-white">
                            <ul class="menu">
                            	<li class="mr-5 active"><a href="<?php echo URLROOT; ?>">Home</a></li>
                            	<li class="mr-5"><a href="<?php echo URLROOT; ?>/pages/about">About Us</a></li>
                            	<li class="mr-5"><a href="<?php echo URLROOT; ?>/pages/services/backdrop/1">Services</a></li>
                            	<li class="mr-5"><a href="<?php echo URLROOT; ?>/pages/ourworks">Our Works</a></li>
                            	<li class="mr-5"><a href="<?php echo URLROOT; ?>/pages/contactus"">Contact Us</a></li>
                                <li><a href="#">Whatsapp</a></li>
                            </ul>
                        </nav>
                        <div class="btn-menu">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>