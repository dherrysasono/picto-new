<!-- Sidebar -->
<ul class="sidebar navbar-nav bg-navyblue">
    <li class="nav-item <?php echo $data['title'] == "Dashboard" ? "active" : ""; ?>">
        <a class="nav-link" href="<?php echo URLROOT; ?>/dashboards">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown <?php echo $data['title'] == "Services" ? "active show" : ""; ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Services</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/whatwedos">What We Do</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/ourvalues">Our Values</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/BackdropMaterial">Backdrop Material & Module</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/construction">Construction Project</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/recover">Recover Material Project</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/workshop">Our Workshop & Tools</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/products">Our Product</a>
            <a class="dropdown-item" href="<?php echo URLROOT; ?>/machine">Our Machine</a>
        </div>
    </li>
</ul>