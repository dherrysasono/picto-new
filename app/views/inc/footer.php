            <!-- Footer -->
            <footer class="footer widget-footer">
                <div class="container">
                    <div class="row"> 
                        <div class="col-lg-4 col-sm-6 reponsive-mb30">  
                            <div class="widget-logo">
                                <div id="logo-footer" class="logo">
                                    <a href="index.html" rel="home">
                                        <img src="<?php echo URLROOT;?>/images/logo.png" alt="image">
                                    </a>
                                </div>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p> 
                                <ul class="flat-information">
                                    <li><i class="fa fa-map-marker"></i><a href="#">40 Baria Sreet, NewYork City, US</a></li>
                                    <li><i class="fa fa-phone"></i><a href="#">001-1234-88888</a></li>
                                    <li><i class="fa fa-envelope"></i><a href="#">info.deercreative@gmail.com</a></li>
                                </ul> -->           
                            </div>         
                        </div><!-- /.col-md-3 --> 

                        <div class="col-lg-4 col-sm-6 reponsive-mb30">  
                            <div class="widget widget-out-link clearfix">
                                <h5 class="widget-title">Our Links</h5>
                                <ul class="one-half">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="services.html">Services</a></li>
                                </ul>
                                <ul class="one-half">
                                    <li><a href="new-fullwidth.html">Our Works</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </div>
                        </div><!-- /.col-md-3 -->

                        <div class="col-lg-4 col-sm-6 reponsive-mb30">
                            <div class="widget widget-letter">
                                <h5 class="widget-title">Download Our E-Company Proﬁle</h5>
                                <form id="subscribe-form" class="flat-mailchimp" method="post" action="#" data-mailchimp="true">
                                    <div class="field clearfix" id="subscribe-content"> 
                                        <p class="wrap-input-email">
                                            <input type="text" tabindex="2" id="subscribe-email" name="subscribe-email" placeholder="Enter Your Email">
                                        </p>
                                        <center>
                                            <button type="button" id="subscribe-button" class="btn btn-warning">
                                                GET NOW
                                            </button>
                                        </center>
                                    </div>
                                    <div id="subscribe-msg"></div>
                                </form>
                            </div>
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->    
                </div><!-- /.container -->
            </footer>

            <!-- Bottom -->
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright text-center"> 
                                <p>@2019 <a href="https://themeforest.net/user/themesflat">Pictograph Advertising</a>. All rights reserved.
                                </p>
                            </div>
                        </div>
                    </div>          
                </div><!-- /.container -->
            </div><!-- bottom -->

            <!-- Go Top -->
            <a class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>

        <!-- Core jQuery Script -->
        <script src="<?php echo URLROOT; ?>/js/jquery-front.min.js"></script>

        <script src="<?php echo URLROOT; ?>/js/tether.min.js"></script>
        <script src="<?php echo URLROOT; ?>/js/bootstrap.min.js"></script> 
        <script src="<?php echo URLROOT; ?>/js/jquery.easing.js"></script>    
        <script src="<?php echo URLROOT; ?>/js/jquery-waypoints.js"></script>    
        <script src="<?php echo URLROOT; ?>/js/jquery-validate.js"></script> 
        <script src="<?php echo URLROOT; ?>/js/jquery.cookie.js"></script>
        
        <script src="<?php echo URLROOT; ?>/js/owl.carousel.min.js"></script>

        <?php if(isset($data['color'])): ?>
        <script src="<?php echo URLROOT; ?>/js/modern.custom.js"></script>
        <script src="<?php echo URLROOT; ?>/js/jquery.hoverdir.js"></script>
        <?php else: ?>
        <script src="<?php echo URLROOT; ?>/js/jquery.flexslider-min.js"></script>
        <script src="<?php echo URLROOT; ?>/js/headline.js"></script>
        <?php endif; ?>
    
        <script src="<?php echo URLROOT; ?>/js/gmap3.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtwK1Hd3iMGyF6ffJSRK7I_STNEXxPdcQ&region=GB"></script>
        <script src="<?php echo URLROOT; ?>/js/parallax.js"></script>
        <script src="<?php echo URLROOT; ?>/js/main.js"></script>

        <!-- Slider Revolution core JavaScript files -->
        <script src="<?php echo URLROOT; ?>/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/slider.js"></script>

        <!-- Slider Revolution extension scripts. ONLY NEEDED FOR LOCAL TESTING --> 
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="<?php echo URLROOT; ?>/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>

        <script src="<?php echo URLROOT; ?>/js/picto.js"></script>
    </body>
</html>