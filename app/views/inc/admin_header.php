<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Minor Solution">

        <title><?php echo $data['title']; ?></title>

        <!-- Bootstrap core CSS-->
        <link href="<?php echo URLROOT; ?>/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="<?php echo URLROOT; ?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <link href="<?php echo URLROOT; ?>/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo URLROOT; ?>/css/sb-admin.css" rel="stylesheet">

        <?php
        if (isset($data['stylesheet'])) {
            foreach ($data['stylesheet'] as $css) {
                echo '<link href="'.URLROOT.'/css/'.$css.'" rel="stylesheet">';
            }
        }
        ?>
    </head>
    <body id="page-top">
        <?php require APPROOT .'/views/inc/admin_navbar.php'; ?>
        <div id="wrapper">
            <?php require APPROOT .'/views/inc/admin_sidebar.php'; ?>
            <div id="content-wrapper">