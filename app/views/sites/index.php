<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="<?php echo URLROOT; ?>/sites" class="btn btn-light"><i class="fa fa-backward">Back</i></a>
<br>
<p>Master Site </p>
<?php foreach($data['sites'] as $site) : ?>
<div class="card card-body mb-3">
  <h1><?php echo $site->siteName; ?> </h1>
  <p><?php echo $site->location; ?> </p>
</div>
<?php endforeach; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>