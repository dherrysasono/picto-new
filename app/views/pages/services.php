<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="rev_slider_wrapper fullwidthbanner-container">
    <div id="services_slider" class="rev_slider fullwidthabanner" data-version="5.4.5" style="display:none">
        <ul>
            <li data-transition="fade">
                <img src="<?php echo URLROOT; ?>/images/slides/1.jpg" alt="Sky" class="rev-slidebg">
            </li>
            <li data-transition="fade">
                <img src="<?php echo URLROOT; ?>/images/slides/2.jpg" alt="Sky" class="rev-slidebg">
            </li>
        </ul>
    </div>
</div>

<section class="flat-row section-icon">
    <div class="container">
        <?php if ($data['serviceTypes']): ?>
        <ul id="data-effect" class="data-effect wrap-iconbox margin-top_121 clearfix">
            <?php foreach($data['serviceTypes'] as $row): ?>
            <?php $url = URLROOT . "/pages/services/" . $row->functionAction . "/" . $row->categoryID; ?>
            <li>
                <a href="<?php echo $url; ?>">
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_globe"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title" <?php if ($url == $data['currentUrl']) echo 'style="color:#fff"'; else echo ""; ?>>
                                <?php echo $row->categoryName; ?>
                            </h5>
                        </div>
                        <div class="effecthover" <?php if ($url == $data['currentUrl']) echo 'style="display: block; left: 0px; top: 0px; transition: all 300ms ease 0s;"'; else echo ""; ?> >
                            <img src="<?php echo URLROOT; ?>/images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        <div class="divider sh72"></div>
    </div>
</section>

<?php if($data['whatwedo']): ?>
<section class="flat-row section-services2">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="<?php echo URLROOT . $data['whatwedo']->imageDesc; ?>" class="img-fluid">
            </div>
            <div class="col-md-8">
                <div class="title-section style3 blueborder left">
                    <h1 class="title" style="color: #1b1b1d;"><?php echo $data['whatwedo']->servicesTitle; ?></h1>
                    <div class="sub-title text-justify" style="padding-left: 0px;">
                        <?php echo $data['whatwedo']->servicesDesc; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if($data['ourvalues']): ?>
<section class="flat-row v3 parallax parallax4">
    <div class="section-overlay style2"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section style3 whiteborder text-center">
                    <h1 class="title" style="color: #fff;"><?php echo $data['ourvalues'][0]->servicesTitle; ?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach($data['ourvalues'] as $ourvalue): ?>
            <div class="col-lg-6">
                <div class="iconbox iconleft style2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <p class="text-justify" style="color: #fff"><?php echo $ourvalue->servicesDesc; ?></p> 
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php' ; ?>