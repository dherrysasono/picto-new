<?php require APPROOT . '/views/inc/header.php'; ?>

<!-- Page title -->
<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="page-title-heading">
                    <h1 class="title">About Us</h1>
                </div><!-- /.page-title-captions -->  
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                        <li>About Us</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title --> 

<section class="flat-row  page-teammember">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="<?php echo URLROOT; ?>/images/services/about.jpg" class="img-fluid" alt="Responsive image">
            </div>
            <div class="col-md-7">
                <div class="title-section style3 yellowborder left">
                    <h1 class="title picto-text">OUR WORKS</h1>
                    <p class="text-justify picto-text">
                        Berdasarkan pengalaman kerja yang cukup lama di industri periklanan terutama di bidang reklame luar ruang, Andrian Tedjo Wibowo melihat peluang yang besar dan memiliki ide untuk mendirikan perusahaan sendiri yang bergerak dalam bidang pengerjaan media promosi baik outdoor maupun indoor, yang kemudian diberi brand “Pictograph”
                        <br><br>
                        Pictograph sendiri diambil dari istilah piktogram yaitu sebuah representatif dari tulisan atau kata yang diimplementasikan kedalam gambar sehingga lebih memudahkan dalam penyampaian pesan.
                        <br><br>
                        oleh karena itu, berharap Pictograph dapat menjadi perusahaan periklanan yang  dapat membantu kegiatan promosi client secara efektif dan eﬁsien dengan komunikasi yang baik sehingga kepuasan pelanggan dapat tercapai.
                        <br><br>
                        Saat ini Pictograph memiliki naungan badan hukum atas nama CV. Media kreatif utama yang secara resmi sudah terdaftar di kantor pajak dan sudah menjadi Perusahaan Kena Pajak (PKP). 
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="flat-row padingbotom">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 history-text">
                <div class="title-section style3 yellowborder left">
                    <h1 class="title">Our Milestone</h1>
                </div>
                <p style="color:#12172B;" class="text-justify">
                Seiring berjalannya waktu, Pictograph mengalami perkembangan yang cukup pesat terutama dari keberhasilan mendapatkan kepercayaan client besar untuk mengerjakan project mereka. kemudian dari sisi penambahan karyawan dan kantor baru yang lebih nyaman.
                <br><br>
                Pada tahun 2018, persaingan usaha yang semakin ketat membuat Pictograph memiliki inisiatif untuk membuka lini bisnis baru yaitu sewa dan produksi backdrop & flooring sehingga dapat memberikan kemudahan kepada client existing atau potential client untuk support event mereka.
                </p>
            </div>
            <div class="col-lg-6">
                <div class="main-history">
                    <div class="wrap-step clearfix">
                        <div class=" data-step float-left">
                            <span class="year picto-text">2014</span>
                        </div>
                        <div class=" info-step float-left">
                            <h5>Pictograph Berdiri</h5>
                            <p>
                            Pictograph secara resmi berdiri sebagai perusahaan advertising yang menangani pekerjaan produksi kerangka reklame, cetak & pemasangan media outdoor - indoor dan efektif mulai beroperasi pada tanggal 01 November 2014.
                            </p>
                        </div>
                    </div>
                    <div class="wrap-step clearfix">
                        <div class=" data-step float-left">
                            <span class="year">2015</span>
                        </div>
                        <div class=" info-step float-left">
                            <h5>Mendapatkan Client Besar</h5>
                            <p class="text-justify picto-text">
                            Pictograph sukses mendapatkan client dari berbagai industri terutama dari sektor properti seperti PT Metropolitan Land (Grand Metropolitan Mall), PT Bumi Parama Wisesa (Navapark), Vida Bekasi, JYSK, Starbucks Coffee, Maxx Coffee, Gold’s Gym, dll.
                            </p>
                        </div>
                    </div>
                    <div class="wrap-step clearfix">
                        <div class=" data-step float-left">
                            <span class="year">2016</span>
                        </div>
                        <div class=" info-step float-left">
                            <h5>Kantor & Karyawan Baru</h5>
                            <p>
                            Pictograph menyewa kantor di Kawasan komersial ruko emerald Summarecon Bekasi dan menambah karyawan untuk divisi operasional lapangan, administrasi & keuangan
                            </p>
                        </div>
                    </div>
                    <div class="wrap-step clearfix">
                        <div class=" data-step float-left">
                            <span class="year">2017</span>
                        </div>
                        <div class=" info-step float-left">
                            <h5>Berbadan Hukum dengan Nama CV. Media Kreatif Utama</h5>
                            <p>
                            CV. Media Kreatif Utama adalah badan hukum yang menaungi brand Pictograph Advertising dan sudah resmi menjadi perusahaan kena pajak (PKP).
                            </p>
                        </div>
                    </div>
                    <div class="wrap-step clearfix">
                        <div class=" data-step float-left">
                            <span class="year">2018</span>
                        </div>
                        <div class=" info-step float-left">
                            <h5>Membuka Lini Bisnis Baru</h5>
                            <p>
                            Pictograph membuka lini bisnis baru sewa dan produksi Backdrop & Flooring.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="flat-row  page-teammember">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section style3 text-center">
                    <h1 class="title">Meet Our Team</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/1.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Andrian Tedjo W</h5>
                        <p class="position">Founder & CEO</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/2.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Eva Pustika</h5>
                        <p class="position">Finance & Admin</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/3.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Ferdinand Aldry</h5>
                        <p class="position">Sales Consultant</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/4.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Ade Mulyana</h5>
                        <p class="position">Koordinator Lapangan</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/4.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Aji Satriana</h5>
                        <p class="position">Staff Lapangan</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="flat-team">                            
                    <div class="avatar">             
                        <img src="<?php echo URLROOT; ?>/images/team/4.jpg" alt="image">
                    </div>                        
                    <div class="content text-center" style="border: 0px solid #ebebeb;">
                        <h5 class="name">Aji Satriana</h5>
                        <p class="position">Staff Lapangan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require APPROOT . '/views/inc/footer.php' ; ?>