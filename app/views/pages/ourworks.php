<?php require APPROOT . '/views/inc/header.php'; ?>

<!-- Page title -->
<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="page-title-heading">
                    <h1 class="title" style="font-weight: bold;">Our Works</h1>
                </div><!-- /.page-title-captions -->  
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                        <li><a href="projects.html">Our Works</a></li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title -->

<!-- Blog posts -->
<section class="flat-row flat-project-v1">
    <div class="container">
        <div class="post-wrap project-v1 post-list clearfix">
            <article class="entry border-shadow clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                        	<img src="<?php echo URLROOT; ?>/images/ourworks/rups-lippo-karawaci.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post">
                        	<a href="single-post.html">Acara Rapat umum Pemegang Sahan (RUPS) Lippo Karawaci </a>
                        </h2>
                        <div class="row picto-text">
                        	<div class="col-sm-2">
                        		<p>Client</p>
                        		<p>Project</p>
                        		<p>Location</p>
                        		<p>Description</p>
                        		<p>Date</p>
						    </div>
						    <div class="col-sm-1">
						      <p>:</p>
						      <p>:</p>
						      <p>:</p>
						      <p>:</p>
						      <p>:</p>
						    </div>
						    <div class="col-sm-9">
						    	<p>Lippo Karawaci, Tbk</p>
						    	<p>Sewa Backdrop & Flooring</p>
						    	<p>Hotel Aryaduta, Jakarta</p>
						    	<p>Backdrop P: 7m T: 3m & Flooring P: 7m L: 1,5m T: 10cm</p>
						    	<p>05 Juni 2018</p>
						    </div>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
        </div>
    </div>

    <div class="container">
        <div class="post-wrap post-grid wrap-column clearfix">
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
            <article class="entry border-shadow flat-column3 clearfix">
                <div class="entry-border clearfix">
                    <div class="featured-post">
                        <a href="single-post.html"> 
                            <img src="<?php echo URLROOT; ?>/images/ourworks/mesjid-lippo-cikarang.jpg" alt="image">
                        </a>
                    </div><!-- /.feature-post -->
                    <div class="content-post">
                        <span class="category">Backdrop & Flooring</span>
                        <h2 class="title-post"><a href="single-post.html">Launching Masjid Terbaru Lippo Cikarang</a></h2>
                        <div class="meta-data style2 clearfix">
                            <ul class="meta-post clearfix">
                                <li class="day-time">
                                    <span>30 Agustus 2018</span>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.contetn-post -->
                </div><!-- /.entry-border -->
            </article>
        </div>
        <div class="blog-pagination clearfix">
            <ul class="flat-pagination  float-left clearfix">
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li class="next"><a href="#">Next<i class="fa fa-angle-double-right"></i></a></li>                               
            </ul><!-- /.flat-pagination -->
            <div class="count-page float-right">
                <span>Page 1 of 60 results</span>
            </div>
        </div><!-- /.blog-pagination -->
    </div>
</section>

<section class="flat-row section-client bg-section">
    <div class="container">
        <div class="row">       
            <div class="col-md-12">
                <div class="flat-client" data-item="5" data-nav="false" data-dots="false" data-auto="true">
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/1.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/2.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/3.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/4.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/5.jpg" alt="image"></div>
                </div><!-- /.flat-client -->      
            </div>
        </div>
    </div>             
</section>

<?php require APPROOT . '/views/inc/footer.php' ; ?>