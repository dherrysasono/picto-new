<?php require APPROOT . '/views/inc/header.php'; ?>

<!-- Page title -->
<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="page-title-heading">
                    <h1 class="title" style="font-weight: bold;">Contact Us</h1>
                </div><!-- /.page-title-captions -->  
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                        <li><a href="projects.html">Contact Us</a></li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title -->

<section class="flat-row page-contact">
    <div class="container">
        <div class="wrap-infobox" style="border-bottom: 0px solid #ebebeb; margin-bottom: 0px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-maps box-shadow3 margin-bottom-73">
                        <div class="maps2" style="width: 100%; height: 520px; "></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="wrap-formcontact">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-section style3 yellowborder left">
                        <h1 class="title picto-text">Messages Us</h1>
                        <p class="text-justify picto-text">
                            Untuk pertanyaan lebih lanjut tentang perusahaan atau layanan kami, silakan isi data dibawah ini dan akan kami respon sesegera mungkin
                        </p>
                    </div>
                    <div class="wrap-formrequest">
                        <form id="contactform" class="contactform wrap-form clearfix" method="post" action="" novalidate="novalidate">
                            <span class="flat-input"><input name="nama" type="text" value="" placeholder="Nama" required="required"></span>
                            <span class="flat-input"><input name="telp" type="text" value="" placeholder="No Telp" required="required"></span>
                            <span class="flat-input"><input name="email" type="email" value="" placeholder="Email" required="required"></span>
                            <span class="flat-input">
                                <button name="submit" type="submit" class="btn btn-warning" id="submit" title="Submit now">SUBMIT</button>
                            </span>
                            <span class="flat-input" style="margin-top: 10px; width: 75%;">
                                <textarea name="pesan" placeholder="Pesan"></textarea>
                            </span> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php' ; ?>