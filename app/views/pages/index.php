<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="container-fluid" style="padding-right: 0px; padding-left: 0px">
    <div class="bd-example">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo URLROOT; ?>/images/slides/slide 1.png" class="d-block w-100">
                    <div class="carousel-caption d-none d-md-block">
                        <h2 class="mb-1" style="color:#E6A31E; font-weight: bold;">BACKDROP & FLOORING</h2>
                        <p class="w-50 text-justify mb-3">Kami menggunakan konstruksi besi pada kerangka Backdrop dan Flooring sehingga menjadikan produk kami lebih kuat dan rapih</p>
                        <button type="button" class="btn btn-warning btn-sm">Read more</button>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo URLROOT; ?>/images/slides/slide 1.png" class="d-block w-100">
                    <div class="carousel-caption d-none d-md-block">
                        <h2 class="mb-1" style="color:#E6A31E; font-weight: bold;">SLIDE 2</h2>
                        <p class="w-50 text-justify mb-3">Kami menggunakan konstruksi besi pada kerangka Backdrop dan Flooring sehingga menjadikan produk kami lebih kuat dan rapih</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo URLROOT; ?>/images/slides/slide 1.png" class="d-block w-100">
                    <div class="carousel-caption d-none d-md-block">
                        <h2 class="mb-1" style="color:#E6A31E; font-weight: bold;">SLIDE 3</h2>
                        <p class="w-50 text-justify mb-3">Kami menggunakan konstruksi besi pada kerangka Backdrop dan Flooring sehingga menjadikan produk kami lebih kuat dan rapih</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<section class="flat-row v3 parallax parallax4">
    <div class="section-overlay style2"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">  
                <div class="title-section style2 color-white text-center">
    				<button type="button" class="btn btn-warning mb-3">
    					Kami Akan Menghubungi Anda
    				</button>
	    			<p class="call-me mt-2">
						Apakah anda ingin berbicara dengan sales representatif kami ? Silahkan isi nama & no tlp, kami akan segera menghubungi anda
					</p>
                </div>

                <div class="wrap-formrequest">
                    <form id="contactform" class="contactform wrap-form clearfix" method="post" action="" novalidate="novalidate">
                        <span class="flat-input"><input name="name" type="text" value="" placeholder="Nama" required="required"></span>
                        <span class="flat-input"><input name="phone" type="text" value="" placeholder="No Telp" required="required"></span>
                        <span class="flat-input"><input name="phone" type="text" value="" placeholder="Email" required="required"></span>
                        <span class="flat-input">
                            <button name="submit" type="submit" class="btn btn-warning" id="submit" title="Submit now">SUBMIT</button>
                        </span>
                    </form>
                </div>         
            </div> 
        </div>
        
    </div>
</section>

<section class="flat-row section-services2">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-section style3 blueborder left">
                    <h1 class="title" style="color: #1b1b1d;">ABOUT US</h1>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<div class="embed-responsive embed-responsive-16by9">
  					<iframe width="560" height="315" src="https://www.youtube.com/embed/thffpTb68P0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
        	</div>
        	<div class="col-md-8">
        		<div class="title-section padding-left50">
        			<h4 class="font-weight-bold">SELAMAT DATANG DI PICTOGRAPH</h4>
                    <div class="sub-title style3 text-justify">
                        Kami adalah perusahaan advertising yang berpengalaman di bidang media promosi, baik itu dalam ataupun luar ruang. Komitmen kami dalam mewujudkan kepuasan client adalah hal yang utama, maka dari itu kami selalu memprioritaskan dari segi kualitas, pelayanan dan harga. Let’s connect with us !
                    </div><br/>
                    <button class="btn btn-warning btn-sm">read more</button>
                </div>
        	</div>
        </div>
    </div>
</section>

<section class="flat-row v3 parallax parallax4">
    <div class="section-overlay style2"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 reponsive-onehalf">  
               <div class="title-section style3 whiteborder left">
                    <h1 class="title" style="color: #fff;">OUR WORKS</h1>
                </div>
            </div>
            <div class="col-md-6 reponsive-onehalf">
                <div class="btn-showall float-right">
                    <a href="projects.html" class="btn btn-warning">Show All Projects</a>
                </div>
            </div> 
        </div>
    </div>
    <div class="owl-carousel owl-theme">
        <div class="item">
            <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
            <p>Test</p>
        </div>
        <div class="item">
            <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
        </div>
        <div class="item">
            <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
        </div>
        <div class="item">
            <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
        </div>
        <div class="item">
            <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
        </div>
    </div>
    <!-- <div class="wrap-imagebox flat-carousel" data-item="5" data-nav="false" data-dots="false" data-auto="false"> 
        <div class="imagebox">
            <div class="imagebox-image"> 
                <a href="single-post.html">
                    <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
                </a> 
            </div> 
            <div class="imagebox-content">
                <p class="imagebox-category">Backdrop PT. Lippo Karawaci (Acara RUPS)  </p>
            </div>
        </div>
        <div class="imagebox">
            <div class="imagebox-image"> 
                <a href="single-post.html">
                    <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
                </a> 
            </div> 
            <div class="imagebox-content">
                <p class="imagebox-category">Backdrop PT. Lippo Karawaci (Acara RUPS)  </p>
            </div>
        </div>
        <div class="imagebox">
            <div class="imagebox-image"> 
                <a href="single-post.html">
                    <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
                </a> 
            </div> 
            <div class="imagebox-content">
                <p class="imagebox-category">Backdrop PT. Lippo Karawaci (Acara RUPS)  </p>
            </div>
        </div>
        <div class="imagebox">
            <div class="imagebox-image"> 
                <a href="single-post.html">
                    <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
                </a> 
            </div> 
            <div class="imagebox-content">
                <p class="imagebox-category">Backdrop PT. Lippo Karawaci (Acara RUPS)  </p>
            </div>
        </div>
        <div class="imagebox">
            <div class="imagebox-image"> 
                <a href="single-post.html">
                    <img src="<?php echo URLROOT; ?>/images/imagebox/s2.jpg" alt="image">
                </a> 
            </div> 
            <div class="imagebox-content">
                <p class="imagebox-category">Backdrop PT. Lippo Karawaci (Acara RUPS)  </p>
            </div>
        </div>
    </div> -->
</section>

<section class="flat-row section-testimonials2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">  
                <div class="title-section text-center">
                    <div class="symbol">
                       ​‌“ 
                    </div>
                    <h1 class="cd-headline clip is-full-width">
                        <span class="cd-words-wrapper">
                            <b class="is-visible" style="font-weight: 500;"> Happy Client</b>
                        </span>
                    </h1>
                </div>      
            </div>
            <div class="col-md-12">
                <div class="wrap-testimonial padding-lr79">
                    <div id="testimonial-slider">
                        <ul class="slides">
                            <li>
                                <div class="testimonials style3 text-center"> 
                                    <div class="message">                                
                                        <blockquote class="whisper">
                                           Kami adalah perusahaan advertising yang berpengalaman di bidang media promosi, baik itu dalam ataupun luar ruang. Komitmen kami dalam mewujudkan kepuasan client adalah hal yang utama, maka dari itu kami selalu memprioritaskan dari segi kualitas, pelayanan dan harga. Let’s connect with us !
                                        </blockquote>
                                    </div>
                                    <div class="avatar">
                                        <span class="name">Irwan Mulyana</span><br>
                                        <div class="start">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <span class="position">
                                            Marketing Manager <br/> LRT City Sentul</span>
                                    </div>                      
                                </div>
                            </li>
                           <li>
                                <div class="testimonials style3 text-center"> 
                                    <div class="message">                                
                                        <blockquote class="whisper">
                                           " We worked with Consuloan. Our representative was  very knowledgeable and helpful. Consuloan made a number of suggestions to help improve our systems. Consuloan explained how things work and why it would help. We are pleased with the result and we definitely highly recommend Consuloan."
                                        </blockquote>
                                    </div>
                                    <div class="avatar">
                                        <span class="name">Alex Poole</span><br>
                                        <div class="start">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <span class="position">CEO DeerCreative</span>
                                    </div>                      
                                </div>
                            </li>
                            <li>
                                <div class="testimonials style3 text-center"> 
                                    <div class="message">                                
                                        <blockquote class="whisper">
                                           " Even though I am a seasoned business owner myself, I am sure that there’s always room for growth, inspiration, and new ideas.It's has provided a common language that is gaining popularity in the workplace as it creates new learning and sets people up for success."
                                        </blockquote>
                                    </div>
                                    <div class="avatar">
                                        <span class="name">Anthony Jones</span><br>
                                        <div class="start">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <span class="position">Business Planner</span>
                                    </div>                      
                                </div>
                            </li>
                        </ul>
                    </div>
                        
                    <div id="testimonial-carousel">
                        <ul class="slides clearfix">
                            <li>
                                <img alt="image" src="images/testimonial/1.jpg">
                            </li>  
                            <li>
                                <img alt="image" src="images/testimonial/2.jpg">
                            </li> 
                            <li>
                                <img alt="image" src="images/testimonial/3.jpg">
                            </li>    
                        </ul>
                    </div>
                </div><!-- /.wrap-testimonial -->
            </div>
        </div>
    </div>
</section>

<section class="flat-row section-client bg-section">
    <div class="container">
        <div class="row">       
            <div class="col-md-12">
                <div class="flat-client" data-item="5" data-nav="false" data-dots="false" data-auto="true">
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/1.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/2.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/3.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/4.jpg" alt="image"></div>
                    <div class="client"><img src="<?php echo URLROOT; ?>/images/clients/5.jpg" alt="image"></div>
                </div><!-- /.flat-client -->      
            </div>
        </div>
    </div>             
</section>
<?php require APPROOT . '/views/inc/footer.php' ; ?>