<?php require APPROOT . '/views/inc/admin_header.php'; ?>

<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Master Project</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="<?=URLROOT."/projects";?>">Master Project</a></li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <?php if ($data['success_message']) {
                        echo "<div class='bs-component'>
                                <div class='alert alert-dismissible alert-success'>
                                    <button class='close' type='button' data-dismiss='alert'>×</button>
                                    " . $data['success_message'] . "
                                </div>
                            </div>";
                    } elseif ($data['error_message']) {
                        echo "<div class='bs-component'>
                                <div class='alert alert-dismissible alert-danger'>
                                    <button class='close' type='button' data-dismiss='alert'>×</button>
                                    " . $data['error_message'] . "
                                </div>
                            </div>";
                    }
                    ?>
                    <a href="<?=URLROOT."/projects/add";?>" type="button" class="btn btn-primary" style="margin-bottom:10px;">Add New</a>
                    <table class="table table-hover table-bordered" id="dtProject">
                        <thead>
                            <tr>
                                <th>Project Code</th>
                                <th>Project Name</th>
                                <th>Remark</th>
                                <th>Active</th> 
                                <th>Insert Time</th>
                                <th>Insert User</th>
                                <th>Last Updated</th>
                                <th>Last Updater</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Confirmation</h5>
            </div>
            <div class="modal-body" id="bodyMessage"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="javascript:void(0)" onclick="deleteProject()" type="button" class="btn btn-primary">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/inc/admin_footer.php' ; ?>