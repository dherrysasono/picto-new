<?php require APPROOT . '/views/inc/admin_header.php'; ?>

<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Master Project</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="<?=URLROOT."/project";?>">Master Project</a></li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="tile">
                <div class="tile-body">
                    <?php if ($data['error_message']) {
                        echo "<div class='bs-component'>
                                <div class='alert alert-dismissible alert-danger'>
                                    <button class='close' type='button' data-dismiss='alert'>×</button>
                                    " . $data['error_message'] . "
                                </div>
                            </div>";
                    }
                    ?>
                    <form id="frmAdd" method="post">
                        <div class="form-group">
                            <label class="control-label">Site</label>
                            <select class="js-example-basic-single form-control" name="siteID">
                                <?php
                                if ($data['sites']) {
                                    foreach($data['sites'] as $site) {
                                        echo '<option value="'.$site->siteID.'">'.$site->siteName.'</option>';
                                    }
                                } 
                                ?>                   
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Project Code</label>
                            <input type="text" class="form-control" name="projectCode" readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Project Name</label>
                            <input type="text" class="form-control" name="projectName" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Remarks</label>
                            <textarea name="remarks" class="form-control"></textarea>
                        </div>
                    </form>
                </div>
                <div class="tile-footer">
                    <button class="btn btn-info" type="submit" form="frmAdd">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>Save
                    </button>
                    <a class="btn btn-primary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                </div>
            </div>
        </div>
    </div>
</main>

<?php require APPROOT . '/views/inc/admin_footer.php' ; ?>