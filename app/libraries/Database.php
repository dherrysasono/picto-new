<?php

/*
 * PDO database Class
 * connect to database
 * create prepared statements
 * Bind values
 * return rows and results
 */

 class Database{
     private $host = DB_HOST;
     private $user = DB_USER;
     private $pass = DB_PASS;
     private $dbname = DB_NAME;

     private $dbHandler;
     private $statement;
     private $error;

     public function __construct(){
        // set DSN MYSQL
        $dsn= 'mysql:host=' . $this->host. ';dbname=' .$this->dbname;

        // Set DSN Postgresql
        // $dsn= 'pgsql:host=' . $this->host. ';dbname=' .$this->dbname;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        // Create PDO Instance
        try{
            $this->dbHandler = new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e){
            $this->error = $e->getMessage();
            echo $this->error;
        }
     }


     // Prepare statement with query
     public function query($sql){
        $this->statement = $this->dbHandler->prepare($sql);
     }

     // Bind Values
     public function bind($param, $value ,$type = null){
         if(is_null($type)){
            switch(true){
                case is_int($value):
                $type =PDO::PARAM_INT;
                break;
                case is_bool($value):
                $type =PDO::PARAM_BOOL;
                break;
                case is_null($value):
                $type =PDO::PARAM_NULL;
                break;
                default:
                $type =PDO::PARAM_STR;
            }
         }

         $this->statement->bindValue($param, $value, $type);
     }

     // Execute the prepared statement
     public function execute($data = array()) 
     {
        if (!empty($data)) {
            return $this->statement->execute($data);
        } else {
            return $this->statement->execute();
        }
        
     }

     // get result set as array of object
     public function resultSet(){
         $this->execute();
         return $this->statement->fetchAll(PDO::FETCH_OBJ);
     }

     // get result set as array of array
     public function resultSetArray() {
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

     // get single record as object
     public function singleData(){
         $this->execute();
         return $this->statement->fetch(PDO::FETCH_OBJ);
     }

     // get row count 
     public function rowCount(){
         return $this->statement->rowCount();
     }

 }
 
