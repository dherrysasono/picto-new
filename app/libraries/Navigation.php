<?php

class Navigation extends Controller {

	public function __construct()
    {
        
    }

	public function generateMenus()
	{
        $this->menuModel = $this->model('Menu');
        extract($_SESSION);

		$menus = array();
        $parentMenus = $this->menuModel->getParentMenu($project_id, $roles_id);
        $counter = 0;

        foreach ($parentMenus as $parentMenu) {
            $childMenus = $this->menuModel->getParentMenu($parentMenu['projectID'], $parentMenu['rolesID'], $parentMenu['menuID']);

            if ($childMenus) {
                foreach ($childMenus as $childMenu) {
                    $parentMenu['child'][] = $childMenu;
                }
            }

            $menus[$counter] = $parentMenu;

            $counter++;
        }

        return $menus;
	}
}