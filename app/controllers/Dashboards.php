<?php

class Dashboards extends Controller {

    public function __construct()
    {
        if(!isLoggedIn()) {
            redirect('users/login');
        }
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $this->view('dashboard/index', $data);
    }
}