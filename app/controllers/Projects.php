<?php

class Projects extends Controller {

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');

        if(!isLoggedIn()) {
            redirect('users/login');
        }

        // $this->menuModel = $this->model('Menu');
        $this->projectModel = $this->model('Project');
        $this->siteModel = $this->model('Site');

        $navigation = new Navigation;
        $this->data_menu = $navigation->generateMenus();
        $this->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public function index()
    {
        $data['menus'] = $this->data_menu;
        $data['currentUrl'] = $this->currentUrl;

        // flash message
        $data['success_message'] = flashdata('success_message');
        $data['error_message'] = flashdata('error_message');

        // Javascript
        $data['javascript'] = array(
            0 => 'plugins/jquery.dataTables.min.js',
            1 => 'plugins/dataTables.bootstrap.min.js',
            2 => 'loadDatatables.js',
            3 => 'project.js'
        );
        
        $this->view('project/index', $data);
    }

    public function dtProject()
    {
        $dataProject = $this->projectModel->getDataProject();
        $datatableProject = array();

		if ($dataProject) {
			foreach($dataProject as $dataRow) {
                $datatableProject['data'][] = array(
                    0 => $dataRow['projectCode'],
                    1 => $dataRow['projectName'],
                    2 => $dataRow['remarks'],
                    3 => $dataRow['isActive'],
                    4 => date('d M Y H:i:s', strtotime($dataRow['insertTime'])),
                    5 => $dataRow['username'],
                    6 => date('d M Y H:i:s', strtotime($dataRow['lastUpdated'])),
                    7 => $dataRow['username'],
					8 => '<center>
							<a href="'.URLROOT.'/projects/edit/'.$dataRow['projectID'].'">
								<i class="fa fa-pencil" aria-hidden="true" title="Edit Project" ></i>
							</a>
							<a onclick="showModalDelete(\''.$dataRow['projectID'].'\',\''.$dataRow['projectName'].'\')" href="javascript:void(0)">
								<i class="fa fa-trash" aria-hidden="true" title="Delete Project" ></i>
							</a>
                        </center>'
				);
            }

            echo json_encode($datatableProject);
		} else {
			$datatableNews['data'] = '';
			echo json_encode($datatableProject);
		} 
    }

    public function add()
    {
        if($_POST) {
            // mengextract $_POST ke dalam bentuk array
            extract($_POST);

            $data = array(
                'siteID' => $siteID,
                'projectCode' => $projectCode,
                'projectName' => $projectName,
                'remarks' => $remarks,
                'isActive' => '1',
                'insertTime' => date('Y-m-d H:i:s'),
                'insertUserID' => $_SESSION['user_id'],
                'lastUpdated' => date('Y-m-d H:i:s'),
                'lastUpdaterID' => $_SESSION['user_id']
            );

            // check duplicated data
            $duplicate_data = $this->projectModel->getProjectByParams($data);

            if (!$duplicate_data) { // not duplicated
                // generate code
                $lastCode = $this->projectModel->getLastProjectCode();

                if($lastCode) {
                    $prefix = substr($lastCode->projectCode,0,2);  
                    $lastNumber = substr($lastCode->projectCode, 2, 4);
                    $projectCode = $lastNumber + 1;
                } else {
                    $prefix = "PR";
                    $projectCode = "1";
                }

                $projectCode = "0000" . $projectCode;
                $projectCode = substr($projectCode, -4);
                $data['projectCode'] = $prefix . $projectCode;

                // save database
                if($this->projectModel->addProject($data)) {
                    // sukses
                    set_flashdata('success_message','Project Successfull Added');
                    redirect('projects');
                } else {
                    // error
                    set_flashdata('error_message','Data Cannot Be Added');
                }
            } else {
                // duplicated data
                set_flashdata('error_message','Data Duplicated');
            }
        }

        $data['menu'] = $this->data_menu;
        $data['sites'] = $this->siteModel->getSites();
        $data['error_message'] = flashdata('error_message');

        // Javascript
        $data['javascript'] = array(
            0 => 'plugins/jquery.validate.min.js',
            1 => 'plugins/additional-methods.min.js',
            2 => 'formRequiredValidation.js',
            3 => 'project.js'
        );
        
        $this->view('project/add', $data);
    }

    public function edit($projectID)
    {
        if($_POST) {
            extract($_POST);
            
            // save to database
            $data = array(
                'projectID' => $projectID,
                'siteID' => $siteID,
                'projectCode' => $projectCode,
                'projectName' => $projectName,
                'remarks' => $remarks,
                'isActive' => '1',
                'insertTime' => date('Y-m-d H:i:s'),
                'insertUserID' => $_SESSION['user_id'],
                'lastUpdated' => date('Y-m-d H:i:s'),
                'lastUpdaterID' => $_SESSION['user_id']
            );

            // check duplicated data
            $duplicate_data = $this->projectModel->getProjectByParams($data);

            if (!$duplicate_data) { // not duplicated
                // update database
                if ($this->projectModel->updateProject($data)) {
                    // sukses
                    set_flashdata('success_message','Project Successfull Edited');
                    redirect('projects');
                } else {
                    // error
                    set_flashdata('error_message','Data Cannot Update');
                }
            } else {
                // duplicated data
                set_flashdata('error_message','Data Duplicated');
            }
        }

        $data['menu'] = $this->data_menu;
        $data['sites'] = $this->siteModel->getSites();
        $data['dataProject'] = $this->projectModel->getProjectByID($projectID);
        $data['error_message'] = flashdata('error_message');

        // Javascript
        $data['javascript'] = array(
            0 => 'plugins/jquery.validate.min.js',
            1 => 'plugins/additional-methods.min.js',
            2 => 'formRequiredValidation.js',
            3 => 'project.js'
        );
        
        $this->view('project/edit', $data);
    }

    public function delete()
    {
        if ($_POST) {
            extract($_POST);

            $data = array(
                'projectID' => $projectID,
                'isActive' => '0',
                'lastUpdated' => date('Y-m-d'),
                'lastUpdaterID' => $_SESSION['user_id']
            );

            if($this->projectModel->deleteProject($data)) {
                set_flashdata('success_message','Project Successfull Deleted');
            } else {
                set_flashdata('error_message','Project Cannot Be Delete');
            }

            echo json_encode('message');
        }
    }
}