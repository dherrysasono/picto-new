<?php defined('URLROOT') OR exit('No direct script access allowed');

class BackdropMaterial extends Controller {
	private $parentTitle = "Services";
	private $title = "Backdrop Material & Module";

	public function __construct() {
		if(!isLoggedIn()) {
            redirect('users/login'); // redirect to login page if not login
        }

        $this->serviceModel = $this->model('Service');
	}

	public function index() {
		$data['parentTitle'] = $this->parentTitle;
		$data['title'] = $this->title;
		$data['javascript'] = array(
			'loadDatatables.js'
		);
		
		// flash message
        $data['success_message'] = flashdata('success_message');
        $data['error_message'] = flashdata('error_message');

        $this->view('backdropmaterial/index', $data);
	}

	public function edit( $serviceID = "" ) {
		if ( $serviceID != ""  ) {
			$data['parentTitle'] = $this->parentTitle;
			$data['title'] = $this->title;
			$data['services'] = $this->serviceModel->getServiceByID($serviceID);
			$data['javascript'] = array('summernote-bs4.js','summernote-setup.js');
			$data['stylesheet'] = array('summernote-bs4.css');

			$this->view('ourvalues/edit', $data);

			/*echo "<pre>";
			print_r($data['services']);
			exit;*/
		} else {
			$this->view('error/404');
		}
	}
}