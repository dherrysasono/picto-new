<?php

/**
 * 
 */
class Menus extends Controller
{
	
	public function __construct()
	{
		if(!isLoggedIn()) {
            redirect('users/login');
        }

        $navigation = new Navigation;
        $this->data_menu = $navigation->generateMenus();
	}

	public function index()
    {
        $data['menu'] = $this->data_menu;
        $this->view('dashboard/index', $data);
    }
}