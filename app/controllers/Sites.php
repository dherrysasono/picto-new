<?php
    class Sites extends Controller{

        public function __construct(){
            
            $this->siteModel = $this->model('Site');
        }

        public function index(){

            $sites = $this->siteModel->getSites();

            $data =[
                'sites'=> $sites
            ];

            $this->view('sites/index',$data);
        }

        public function add(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data =[
                    'title' => trim($_POST['title']),
                    'body' => trim($_POST['body']),
                    'user_id' => $_SESSION['user_id'],
                    'title_err' => '',
                    'body_err' => ''
                ];
                //validate Data
                if(empty($data['title'])){
                    $data['title_err'] = 'Please enter title';
                }
                if(empty($data['body'])){
                    $data['body_err'] = 'Please enter body text';
                }

                if(empty($data['title_err']) && empty($data['body_err']) ){
                    // validated
                    if($this->siteModel->addSite($data)){
                        flash('post_message','Post Added');
                        redirect('sites');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                   // Load view with error
                   $this->view('sites/add', $data);
                }

            } else {
                $data =[
                    'title'=> '',
                    'body'=>''
                ];

                $this->view('sites/add',$data);
            }
        }

        public function edit($id){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data =[
                    'id' => $id,
                    'title' => trim($_POST['title']),
                    'body' => trim($_POST['body']),
                    'user_id' => $_SESSION['user_id'],
                    'title_err' => '',
                    'body_err' => ''
                ];
                //validate Data
                if(empty($data['title'])){
                    $data['title_err'] = 'Please enter title';
                }
                if(empty($data['body'])){
                    $data['body_err'] = 'Please enter body text';
                }

                if(empty($data['title_err']) && empty($data['body_err']) ){
                    // validated
                    if($this->siteModel->updateSite($data)){
                        flash('post_message','Post Updated');
                        redirect('sites');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                   // Load view with error
                   $this->view('sites/edit', $data);
                }

            } else {
                $post = $this->siteModel->getPostByID($id);

                //check for owner
                if($post->user_id != $_SESSION['user_id']){
                    redirect('sites');
                }
                $data =[
                    'id' => $id,
                    'title'=> $post->title,
                    'body'=>$post->body
                ];

                $this->view('sites/edit',$data);
            }
        }

        public function show($id){
            $site = $this->siteModel->getPostByID($id);
            $data=[
                'site' => $site,
            ];
            $this->view('posts/show', $data);
        }

        public function delete($id){

            if($post->user_id != $_SESSION['user_id']){
                redirect('sites');
            }

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if($this->siteModel->deletePost($id)){
                    flash('post_message','Post Removed');
                    redirect('sites');
                } else {
                    die('Something went wrong');
                }
            } else {
                redirect('sites');
            }
        }

    }