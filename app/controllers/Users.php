<?php

class Users extends Controller {

    public function __construct()
    {
        $this->userModel = $this->model('User');
    } 

   public function register()
   {
        // Check for POST
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process Form 

            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data= [
                'name'=>trim($_POST['name']),
                'email'=>trim($_POST['email']),
                'password'=>trim($_POST['password']),
                'confirm_password'=>trim($_POST['confirm_password']),
                'name_err'=>'',
                'email_err'=>'',
                'password_err'=>'',
                'confirm_password_err'=>''
            ];

            // Validate Email
            if(empty ($data['email'])) {
                $data['email_err'] = 'Please enter email';
            } else {
                // check email
                if($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'Email is already taken';
                }
            }

            // Validate Name
            if(empty ($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            // Validate Password
            if(empty ($data['password'])) {
                $data['password_err'] = 'Please enter password';
            } elseif(strlen($data['password'])<6) {
                $data['password_err'] = 'Password must be at least 6 characters';
            }   

            // Validate Confirm Password
            if(empty ($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please enter confirm password';
            } else {
                if ($data['password'] != $data['confirm_password']) {
                    $data['confirm_password_err'] = 'Password do not match';
                }
            }

            // Make sure error are empty
            if(empty($data['email_err']) && empty($data['name_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])) {
                // hash password
                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);

                // Register user
                if($this->userModel->register($data)) {
                    flash('register_success','You are registered and can log in');
                    redirect('users/login');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load View
                $this->view('users/register',$data);
            }
        } else {
            // Load Form
            $data= [
                'name'=>'',
                'email'=>'',
                'password'=>'',
                'confirm_password'=>'',
                'name_err'=>'',
                'email_err'=>'',
                'password_err'=>'',
                'confirm_password_err'=>''
            ];

            // Load View
            $this->view('users/register',$data);
        }
    }

    public function login() 
    {
        if(isLoggedIn()) {
            redirect('dashboard');
        }

        if($_POST) {
            extract($_POST);

            $data_user = $this->userModel->getAppUserByUsername($username);

            if ($data_user) {
                if (password_verify($password, $data_user->password)) {
                    $this->createUserSession($data_user);
                    redirect('dashboards');
                } else {
                    set_flashdata('login_failed','Incorrect Username or Password');
                }
            } else {
                set_flashdata('login_failed','Incorrect Username or Password');
            }
        }

        // flashdata
        $data['error_message'] = flashdata('login_failed');
        $data['title'] = 'Login';

        // Load View
        $this->view('users/login', $data);
    }
   
    public function createUserSession($user)
    {
        $data = array(
            'userID' => $user->userID,
            'userName' => $user->userName
        );

        set_userdata($data);

        return TRUE;
    }

    public function logout()
    {
        $data = array('userID','userName');
        unset_userdata($data);
        redirect('users/login');
    }

}