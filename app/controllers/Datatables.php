<?php defined('URLROOT') OR exit('No direct script access allowed');

class Datatables extends Controller {

	public function __construct()
	{
		if(!isLoggedIn()) {
            redirect('users/login');
        }

        $this->datatableModel = $this->model('Datatable');
	}

	public function dtWhatWeDo()
	{
		$servicesGroup = 'What We Do';
		$data = $this->datatableModel->getDataServices($servicesGroup);
		$datatable = array();

		if ($data) {
			foreach($data as $row) {
                $datatable['data'][] = array(
                    0 => $row['categoryName'],
                    1 => $row['servicesTitle'],
                    2 => date('d M Y H:i:s', strtotime($row['lastUpdated'])),
                    3 => $row['userName'],
					4 => '<center>
							<a href="'.URLROOT.'/whatwedos/edit/'.$row['servicesID'].'">
								<i class="fas fa-pencil-alt"></i>
							</a>
                        </center>'
				);
            }
		} else {
			$datatable['data'] = '';
		}

		echo json_encode($datatable);
	}

	public function dtOurValues()
	{
		$servicesGroup = 'Our Values';
		$data = $this->datatableModel->getDataServices($servicesGroup);
		$datatable = array();

   		if ($data) {
			foreach($data as $row) {
                $datatable['data'][] = array(
                    0 => $row['categoryName'],
                    1 => $row['servicesDesc'],
                    2 => date('d M Y H:i:s', strtotime($row['lastUpdated'])),
                    3 => $row['userName'],
					4 => '	<a href="'.URLROOT.'/ourvalues/edit/'.$row['servicesID'].'">
								<i class="fas fa-pencil-alt"></i>
							</a>'
				);
            }
		} else {
			$datatable['data'] = '';
		}

		echo json_encode($datatable);
	}

	public function dtBackdropMaterial() {
		$servicesGroup = 'Backdrop Material & Module';
		$data = $this->datatableModel->getDataServices($servicesGroup);
		$datatable = array();

   		if ($data) {
			foreach($data as $row) {
                $datatable['data'][] = array(
                    0 => $row['categoryName'],
                    1 => $row['servicesTitle'],
                    2 => $row['servicesDesc'],
                    3 => date('d M Y H:i:s', strtotime($row['lastUpdated'])),
                    4 => $row['userName'],
                    5 => "<a href='" . URLROOT . "/backdropmaterial/edit/{$row['servicesID']}'>
                    		<i class='fas fa-pencil-alt'></i>
                          </a>"
				);
            }
		} else {
			$datatable['data'] = '';
		}

		echo json_encode($datatable);
	}
}