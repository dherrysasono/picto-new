<?php defined('URLROOT') OR exit('No direct script access allowed');

class Pages extends Controller {

    public function __construct()
    {
        $this->pageModel = $this->model('Page');
        $this->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public function index() 
    {
        $this->view('pages/index');
    }
    
    public function about() 
    {
        $data['title'] = 'About Us';
        $this->view('pages/about', $data);
    }

    public function services($option = 'backdrop', $categoryID) 
    {
        $data['color'] = 7;
        $data['option'] = $option;
        $data['serviceTypes'] = $this->pageModel->getServiceTypes();
        $data['currentUrl'] = $this->currentUrl;
        $data['whatwedo'] = $this->pageModel->getWhatWeDo($categoryID);
        $data['ourvalues'] = $this->pageModel->getOurValues($categoryID);
        $this->view('pages/services', $data);
    }

    public function ourworks() 
    {
        $this->view('pages/ourworks');
    }

    public function contactus() 
    {
        $this->view('pages/contactus');
    }
}