<?php defined('URLROOT') OR exit('No direct script access allowed');

class Whatwedos extends Controller {

	public function __construct()
	{
		if(!isLoggedIn()) {
            redirect('users/login');
        }

        $this->serviceModel = $this->model('Service');
	}

	public function index()
	{
		$data['parentTitle'] = 'Services';
		$data['title'] = 'What We Do';
		$data['javascript'] = array(
			'loadDatatables.js'
		);
		
		// flash message
        $data['success_message'] = flashdata('success_message');
        $data['error_message'] = flashdata('error_message');

        $this->view('whatwedo/index', $data);
	}

	public function edit($serviceID = 0)
    {
    	$data['errors'] = array();
    	
    	if ($_POST) {
    		extract($_POST);

    		$data_services = array(
    			'servicesID' => $serviceID,
    			'servicesTitle' => $servicesTitle,
    			'servicesDesc' => $serviceDescription,
    			'lastUpdated' => date('Y-m-d H:i:s'),
    			'lastUpdaterID' => $_SESSION['userID']
    		);

    		$update_result = $this->serviceModel->updateServices($data_services);

    		if ($update_result) {
    			if(isset($_FILES['image'])) {
    				$path_upload = "./images/services/";
    				$path_save = "/images/services/";
      				$file_name = $_FILES['image']['name'];
      				$file_size =$_FILES['image']['size'];
      				$file_tmp = $_FILES['image']['tmp_name'];
      				$file_type = $_FILES['image']['type'];
      				$file_ext = strtolower($_FILES['image']['name']);
      				$file_ext = explode(".", $file_ext);
      				$file_ext = end($file_ext);

      				$expensions = array("jpeg","jpg","png");
      				$image_info = getimagesize($_FILES["image"]["tmp_name"]);
      				$image_width = $image_info[0];
					$image_height = $image_info[1];

      				if (in_array($file_ext,$expensions) === false) {
         				$data['errors'][] = "extension not allowed, please choose a JPEG or PNG file.";
      				}
      
      				if ($file_size > 2097152) {
         				$data['errors'][] = 'File size must be excately 2 MB';
      				}

      				if ($image_width <= $image_height) {
      					$data['errors'][] = 'File width must be large than height';
      				}
      
      				if (empty($data['errors']) == true) {
         				// upload image
         				move_uploaded_file($file_tmp, $path_upload.$file_name);

         				// check image
         				if (file_exists($path_upload.$file_name)) {
         					// update database
         					$data_servicesimage = array(
				    			'servicesID' => $serviceID,
				    			'imageTitle' => $file_name,
				    			'imageDesc' => $path_save.$file_name,
				    			'lastUpdated' => date('Y-m-d H:i:s'),
				    			'lastUpdaterID' => $_SESSION['userID'],
				    			'imageID' => $imageID
				    		);

				    		$update_servicesimage_result = $this->serviceModel->updateServicesImage($data_servicesimage);

				    		if ($update_result) {
				    			set_flashdata('success_message','Data Successfull Be Updated');
				    		} else {
				    			set_flashdata('error_message','Data Cannot Be Updated');
				    		}

				    		redirect('whatwedos');
         				}
      				}
    			}
    		}
    	}

		if ($serviceID != 0) {
    		// get data services
    		$data['services'] = $this->serviceModel->getServiceByID($serviceID);
    		$data['parentTitle'] = 'What We Do';
			$data['title'] = 'Edit';
			$data['javascript'] = array('summernote-bs4.js','summernote-setup.js');
			$data['stylesheet'] = array('summernote-bs4.css');
    		$this->view('whatwedo/edit', $data);
    	} else {
    		$this->view('error/404');
    	}
    }
}